import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {

  private _menu: Array<string> = [];

  @Input()  
  public selectedItem :string = "";

  constructor() { }

  ngOnInit(): void {
    this._menu.push("Страница 1");
    this._menu.push("Страница 2");
    this._menu.push("Страница 3");
    this._menu.push("Страница 4");
    this._menu.push("Страница 5");

    this.selectedItem = this._menu[0];
  }

  public GetMenuItems(): Array<string> {
    return this._menu;
  }

  public onChange(value:string){
    this.selectedItem = value;
  }
}
